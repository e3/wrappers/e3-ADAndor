# Copyright (C) 2023 European Spallation Source ERIC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile

EXCLUDE_ARCHS += linux-corei7-poky
EXCLUDE_ARCHS += linux-ppc64e6500

SUPPORT:=andorSupport

APP:=andorApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src

## We will use XML2 as the system lib, instead of ADSupport
## Can we use this on this?
## In case, I added the following lines.
ifeq (linux-ppc64e6500, $(findstring linux-ppc64e6500,$(T_A)))
USR_INCLUDES += -I$(SDKTARGETSYSROOT)/usr/include/libxml2
else ifeq (linux-corei7-poky, $(findstring linux-corei7-poky,$(T_A)))
USR_INCLUDES += -I$(SDKTARGETSYSROOT)/usr/include/libxml2
else
USR_INCLUDES += -I/usr/include/libxml2
endif

LIB_SYS_LIBS += xml2	

HEADERS += $(SUPPORT)/ATMCD32D.h
HEADERS += $(SUPPORT)/atmcdLXd.h
HEADERS += $(SUPPORT)/ShamrockCIF.h

DBDS += $(APPSRC)/andorCCDSupport.dbd
DBDS += $(APPSRC)/shamrockSupport.dbd

SOURCES += $(APPSRC)/andorCCD.cpp
SOURCES += $(APPSRC)/shamrock.cpp

ifeq (linux-x86_64, $(findstring linux-x86_64,$(T_A)))
USR_LDFLAGS += -Wl,--enable-new-dtags
USR_LDFLAGS += -L$(where_am_I)/$(SUPPORT)/os/linux-x86_64
USR_LDFLAGS += -Wl,-rpath,"\$$ORIGIN/../../../../../siteLibs/vendor/$(E3_MODULE_NAME)/$(E3_MODULE_VERSION)"
# With Ubuntu, the linker needs the option -Wl,--start-group
# in order to find all undefined symblos.
# So without this option, put the vendor libs in LIB_SYS_LIBS.
# Thus, they will be called in proper oders to be linked.
# 
LIB_SYS_LIBS += andor
LIB_SYS_LIBS += shamrockcif
endif

VENDOR_LIBS += $(SUPPORT)/os/linux-x86_64/libandor-x86_64.so.2.102.30034.0
VENDOR_LIBS += $(SUPPORT)/os/linux-x86_64/libandor.so.2
VENDOR_LIBS += $(SUPPORT)/os/linux-x86_64/libandor.so
VENDOR_LIBS += $(SUPPORT)/os/linux-x86_64/libshamrockcif-x86_64.so.2.102.30034.0
VENDOR_LIBS += $(SUPPORT)/os/linux-x86_64/libshamrockcif.so.2
VENDOR_LIBS += $(SUPPORT)/os/linux-x86_64/libshamrockcif.so

TEMPLATES += $(wildcard $(APPDB)/*.db)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)

USR_DBFLAGS += -I $(E3_SITEMODS_PATH)/adcore/$(call FETCH_REVISION_NUMBER,$(E3_SITEMODS_PATH),adcore)/db

SUBS=$(wildcard $(APPDB)/*.substitutions)
TMPS=$(wildcard $(APPDB)/*.template)

.PHONY: vlibs
vlibs: $(VENDOR_LIBS)

.PHONY: $(VENDOR_LIBS)
$(VENDOR_LIBS):
	@$(SUDO) install -m 755 -d $(E3_MODULES_VENDOR_LIBS_LOCATION)/
	@$(SUDO) install -m 644 $@ $(E3_MODULES_VENDOR_LIBS_LOCATION)/

